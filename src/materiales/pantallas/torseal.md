# Pegamento TorrSeal

La resina Torr Seal mostrada en la imagen comúnmente se utiliza para sellar fugas en sistemas de alto vacío. En este caso, se utiliza para formar la unión de las diferentes patas de la pantalla en forma de varilla y también para una de las uniones de la pantalla "cuchillo de mantequilla". Se cree que se optó por la utilización de esta resina especial en lugar de soldadura por la versatilidad en poder cambiar la configuración de las pantallas (principalmente la de la varilla).

![](resina.jpg)
