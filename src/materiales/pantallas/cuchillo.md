# Pantalla Cuchillo Mantequilla

Esta pantalla está compuesta de secciones lineales y curvas de acero inoxidable. Originalmente, la parte curva de la pantalla tenía un radio de aproximadamente \\( 90 \ mm\\) que es equivalente al radio de la cámara de vacío. No obstante, la pantalla se tuvo que deformar para disminuir su ancho en unas pruebas preliminares, pues esta está al límite del ancho máximo que permite no rallar el interior del puerto.

Dimensiones aproximadas de esta pantalla:
- \\( h_{pantalla} \approx 17 \ cm \\)
- \\( h_{pantalla+base} \approx 27 \ cm \\)
- \\( a_{max} \approx 6,8 \ cm \\)

![](cuchillomantequilla.jpg)
