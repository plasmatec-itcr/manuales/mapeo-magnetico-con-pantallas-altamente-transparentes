# Motor de paso

El movimiento oscilatorio de las pantallas "cuchillo de mantequilla" y la varilla es controlado por un motor de paso. El motor es controlado por el circuito mostrado en la imagen, donde los botones rojo y verde corresponden a apagado y encendido respectivamente, los botones blancos permiten modificar la posición inicial ya sea en forma horaria o antihorario, y los botones azules permiten controlar su velocidad.

Flashear: Botón blanco de RESET Arduino

Pin de dirección = 3
Pin de pasos = 4
Pin horario = 5
Pin antihorario = 6
Pin inicio = 9
Pin calibrar = 8 (¿Apagado?)
Pin rapidez = 12
Pin disminuir velocidad (rapidez minus) = 13
Retraso = 100 s


--Colocar las especificaciones del motor--

--Explicar el funcionamiento del circuito--

![](motor.jpg)


![](circuitoMotor.jpg)


