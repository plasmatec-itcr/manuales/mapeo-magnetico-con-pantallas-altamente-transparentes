# Óxido de Zinc

El óxido de zinc funciona como la sustancia fosforescente que emite luz al reaccionar al paso de los electrones por las líneas de campo magnético del SCR-1. El óxido se debe aplicar a las pantallas utilizando una mezcla con un poco de acetona. 

El manejo de esta sustancia se debe realizar con sumo cuidado, pues es peligroso para el ambiente según la etiqueta que se muestra en su recipiente. Es por ello que existe un beaker separado para su manipulación en el laboratorio, y no se deben desechar de ninguna manera sus residuos.

![](OxidoZinc.jpg)
