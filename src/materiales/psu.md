# Fuentes de poder

El laboratorio dispone de algunas fuentes de alimentación variables ya sea por corriente o por tensión. En este caso se muestra la conexión de tres fuentes de alimentación en serie para obtener una tensión de 150 V aproximadamente. Debido a que algunas de las fuentes del laboratorio no funcionan, se debe verificar que las fuentes que se vayan a utilizar funcionen correctamente antes de hacer el montaje en serie. En las siguientes cuatro imágenes se muestran algunas de las fuentes del laboratorio. Recuerde conectar las fuentes cuando estas estén apagadas y encenderlas una vez finalizada la conexión para medir la tensión.

![](fotosFuentes/Fuentes1.jpg)

![](fotosFuentes/Fuentes2.jpg)

![](fotosFuentes/Fuentes3.jpg)

![](fotosFuentes/Fuentes4.jpg)

En las siguientes dos fotos se muestra una de las fuentes que se encontró que no funciona correctamente, por lo que se procedió a marcarla como "Mala" para diferenciarla de las otras.

![](fotosFuentes/Fuentes5.jpg)

![](fotosFuentes/Fuentes6.jpg)

Debido a  que las mesas del laboratorio que se pueden colocar cerca de los puertos del SCR-1 son pequeñas, las fuentes se deben colocar una encima de la otra junto al multímetro digital de banco como se muestra en la siguiente imagen.

![](fotosFuentes/Fuentes11.jpg)

Como se mencionó anteriormente para obtener una tensión de 150 V, se deben colocar las tres fuentes en serie y por último medir en paralelo la tensión total con el multímetro.

Como se muestra en la siguiente imagen, utilizando cables macho a macho se conectan ambos puertos de la fuente en serie conectando de positivo a negativo como muestra el cable amarillo. Esta conexión permite obtener una tensión teóricamente de 60 V, pues cada puerto de la fuente otorga una tensión de 30 V.

![](fotosFuentes/Fuentes7.jpg)

Después, se coloca el segundo puerto (puerto de la derecha) de la primera fuente al segundo puerto de la segunda fuente. Esta conexión se muestra con el cable celeste en al siguiente imagen, siempre conectando positivo con negativo. Obteniendo así 90 V (teóricamente), se colocan ambos puertos de la fuente en serie como muestra el cable verde.


![](fotosFuentes/Fuentes8.jpg)

Por último, se conecta en serie el primer puerto (puerto de la izquierda) al segundo puerto (puerto de la derecha) de la tercera fuente como se muestra en la siguiente foto. Esta conexión se muestra con un cable rojo, y observe que siempre se realiza la conexión de un puerto a otro de positivo a negativo.

![](fotosFuentes/Fuentes9.jpg)

En la siguiente imagen se muestra como queda el sistema una vez conectadas todas las fuentes en serie para obtener una tensión de aporximadamente 150 V. Para medir esta tensión con el multímetro se sabe que se debe hacer en paralelo, por lo que, como se muestra en la siguiente imagen, se utiliza un cable doble macho a doble lagarto. Tomando el rojo como positivo y el negro como negativo se conecta el doble macho a los puertos libres del sistema, el positivo de la tercera fuente y el negativo de la primera fuente.

![](fotosFuentes/Fuentes10.jpg)

Una vez establecida esta conexión en paralelo, se conectan los lagartos a los dos cables del multímetro como se muestra en la siguiente foto.

![](fotosFuentes/Fuentes12.jpg)

Como se muestra en la siguiente imagen, se procede a conectar los cables al multímetro. Es recomendable utilizar el multímetro digital de banco pues es más preciso y permitiría monitorear la resistencia del sistema en el diagnóstico del campo magnético. Para obtener la tensión solo se coloca en DC V (tensión coriente directa) y utilizar la opción de "Auto" en el rango.

![](fotosFuentes/Fuentes13.jpg)

Para este caso se utilizó el multímetro digital convencional, pues el multímetro de banco no estaba funcionando correctamente. En la siguiente foto se muestra la conexión al multímetro convencional con las fuentes apagadas. Para este multímetro solo se debe colocar el negativo (cable negro) al puerto "COM" y el positivo (cable rojo) al puerto V/\\(\Omega\\), y mover al perilla para hasta el modo tensión.

![](fotosFuentes/Fuentes14.jpg)

En la última imagen se muestra la medición de la tensión total con el multímetro convencional, que fue de aproximadamente 148.1 V.

![](fotosFuentes/Fuentes15.jpg)


 Observe que la conexión mostrada en este proceso se hace de esta manera solo para seguir un orden. Se puede seguir cualquier orden deseado, siempre y cuando las conexiones se realicen de manera correcta, y con un orden claro que permita medir fácilmente la tensión total.


