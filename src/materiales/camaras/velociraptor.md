# Cámara Velociraptor HS

El laboratorio tiene a disposición una cámara velociraptor que captura imágenes en blanco y negro. 

La cámara se conecta por ethernet y se controla por un programa llamado Sharki.


![](imagenes/camaraBN.jpg)

![](imagenes/camaraBN2.jpg)

## Imágenes tomadas por la cámara


