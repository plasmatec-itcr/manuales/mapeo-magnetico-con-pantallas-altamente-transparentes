# Sonda de Langmuir

![](egun2.jpg)

![](egun3.jpg)


## Proceso de ensamble y desensamble

 El ensamble y desensamble de la sonda permite verificar el funcionamiento de sus conexiones. Es importante que se compruebe que los cables correspondientes al filamento de la pistola de electrones estén correctamente soldados a los cables principales de la sonda. <!--preguntar como se pueden llamar estos cables --> Este proceso de verificación de funcionamiento de las conexiones se describirá a continuación.

 En la siguiente imagen se muestra la sonda desde la parte trasera. Para desarmar la sonda se debe desensamblar la parte delantera primero que sostiene las conexiones principales con el filamento y después desatornillar la parte trasera. Desde la parte trasera se saca la parte que contiene las conexiones que forman la pistola de electrones con el filamento.

![](fotosLangmuir/Langmuir1.jpg)

![](fotosLangmuir/Langmuir2.jpg)

![](fotosLangmuir/Langmuir3.jpg)

En la imagen que se muestra a continuación se muestra la parte delantera de la sonda que se debe desatornillar primero.

![](fotosLangmuir/Langmuir4.jpg)

![](fotosLangmuir/Langmuir5.jpg)

![](fotosLangmuir/Langmuir6.jpg)

En la siguiente foto se muestra la parte trasera de la sonda, la cual se debe desensamblar después de la parte delantera para sacar las conexiones principales que alimentan la pistola de electrones. En esta foto ya se quitaron los tornillos que se encuentran más afuera de manera radial.

![](fotosLangmuir/Langmuir7.jpg)

![](fotosLangmuir/Langmuir8.jpg)

![](fotosLangmuir/Langmuir9.jpg)

![](fotosLangmuir/Langmuir10.jpg)

![](fotosLangmuir/Langmuir11.jpg)

Cabe recalcar que en esta parte delantera no todos los tornillos calzan de manera correcta o inclusive tienen funcionalidad alguna. Esto porque, aunque para uno de los tornillos existe una entrada, esta solo es un hueco en la misma pieza que no la ensambla a ninguna otra. No obstante, los otros tornillos permiten que las piezas se mantengan fijas de manera adecuada.

![](fotosLangmuir/Langmuir12.jpg)

Una vez que la parte delantera se haya desensamblado, se debe desatornillar la parte trasera empezando por los tornillos más grandes, los cuáles están más afuera de manera radial, recordando que siempre se deben desatornillar en cruz. Como se muestra en la foto inmediatamente anterior y en la siguientes imágenes, se puede retirar entonces la parte interna de la sonda que contiene las conexiones.

![](fotosLangmuir/Langmuir13.jpg)

![](fotosLangmuir/Langmuir15.jpg)

![](fotosLangmuir/Langmuir16.jpg)

Después se procede a desatornillar los tornillos más pequeños como se muestra en la imagen anterior para sacar los cables y conexiones principales en sí, por lo que queda el tubo que contiene las conexiones vacío como se muestra en las siguientes dos imágenes.

![](fotosLangmuir/Langmuir17.jpg)

![](fotosLangmuir/Langmuir18.jpg)

En las siguientes tres imágenes se muestran las conexiones de la sonda de Langmuir. En estas se pueden observar que los cables del filamento que forma la pistola de electrones (los cuales son rojos) tienen un extremo descubierto (que se ve plateado). Estos extremos deben ser soldados cada uno a un cable de cobre diferente de las conexiones principales de la sonda. Los cables de cobre también tienen una cobertura, por lo que para saber a cuáles habría que soldarlos, solamente se debe observar a cuáles se les ha quitado un poco de su cobertura (han sido lijados) para poder soldarlos en esa sección descubierta.

![](fotosLangmuir/Langmuir19.jpg)

![](fotosLangmuir/Langmuir20.jpg)

![](fotosLangmuir/Langmuir21.jpg)

![](fotosLangmuir/Langmuir22.jpg)

![](fotosLangmuir/Langmuir23.jpg)

Para soldar las los cables (más rojos) del filamento que forma la pistola de electrones a las conexiones principales se debe utilizar el equipo que se muestra en la siguiente imagen. El soldador eléctrico de tipo lápiz que se muestra a la derecha tiene una perilla para ajustar su potencia y un tipo de espuma que permite limpiar la punta del soldador. A la izquierda de la foto se tiene un recipiente circular azul que contiene una goma para también limpiar la punta del soldador, y cable para probar el funcionamiento del soldador. Cabe recalcar que no es recomendable inhalar esta goma al evaporarse por lo que se debe mantener ventilado el lugar y mantener distanciado el soldador al utilizarla. En el centro se encuentra un multímetro el cual se debe utilizar para comprobar las conexiones realizadas con la soldadura.

![](fotosLangmuir/Langmuir24.jpg)

En las siguientes dos imágenes se muestra como fueron soldadas las conexiones para la ocasión en que fueron tomadas estas fotos para el manual.

![](fotosLangmuir/Langmuir25.jpg)

![](fotosLangmuir/Langmuir26.jpg)

En la siguiente foto se muestra por donde deben pasar los cables que forman el sistema principal de la pistola de electrones. Solamente se deben moldear levemente para que pasen por esa sección.

![](fotosLangmuir/Langmuir27.jpg)

Luedo se deben revisar las secciones soldadas, que estén firmes y no se desprendan fácilmente.

Lo más importante de este proceso es verificar que las conexiones realizadas con la soldadura funcionen. Esto se comprueba utilizando alguno de los multímetros del laboratorio. Se coloca el multímetro en modo resitencia y se pone una de las puntas en el extremo de los cables que forman el sistema principal de la pistola de electrones (cables con cobertura roja) y la otra punta en alguno de los dientes del puerto trasero de la sonda, el cual se muestra en la siguiente imagen. Cuando existe una conexión el multímetro suena, por lo que se debe probar con cada uno de los dientes para ambos cables. Para cada cable debe existir una única conexión con alguno de los dientes. Si no se logra encontrar la conexión se debe revisar la soldadura realizada o volver a probar haciendo contacto con cada uno de los dientes nuevamente.

![](fotosLangmuir/Langmuir28.jpg)

Recuerde que en todo este proceso se debe cubrir con aluminio las partes que quedan al descubierto de la sonda como se muestra en la siguientes fotos. 

![](fotosLangmuir/Langmuir29.jpg)

![](fotosLangmuir/Langmuir31.jpg)


![](fotosLangmuir/Langmuir32.jpg)

![](fotosLangmuir/Langmuir33.jpg)

Después de probar las conexiones se deben limpiar las partes externas (como el tubo principal y los puertos donde se ensamblan las piezas) con acetona utilizando papel. Para volver a ensamblar la sonda se vuelve a realizar el mismo proces, pero de atrás hacia adelante recordando siempre atornillar en cruz.

![](fotosLangmuir/Langmuir34.jpg)

![](fotosLangmuir/Langmuir35.jpg)

Una vez finalizado el desensamble y ensamble de la sonda de Langmuir se cobren las partes de la sonda que quedan expuestas con aluminio para evitar la contaminación. 


