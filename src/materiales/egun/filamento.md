# Filamento
El filamento de la pistola de electrones corresponde al filamento de una bombilla pequeña de tipo de luces de navidad. Para saber si el filamento de la bombilla a utilizar tendrá un funcionamiento medianamente adecuado, se debe medir la resistencia del filamento, la cual debe rondar entre los 2\\(\Omega\\) y 4\\(\Omega\\).

![](bombillafilamento.jpg)

Este filamento debe estar introducido en la pistola de electrones. Para poder introducirlo, se debe romper la parte superior de la bombilla para que solo quede la parte inferior junto al filamento. Esto se puede realizar con alguna de las llaves que se encuentran en el laboratorio.

![](bombillaquebrada.jpg)

![](herramientaparaquebrar.jpg)

Una vez realizado lo anterior, se puede introducir la parte restante de la bombilla en el orificio de la pistola, con lo cual ya se tendría el filamento para el experimento. Se de observar con detenimiento que la parte restante de la bombilla que se introduce en la pistola no pueda romperse con facilidad o esté muy agrietada.

![](pistolaconbombilla.jpg)
