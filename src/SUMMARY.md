# Summary

- [Introducción](intro.md)
- [Materiales](materiales/main.md)
    - [Acetona](materiales/acetona.md)
    - [Pantallas-Targets](materiales/pantallas/main.md)
        - [Pantalla Cuchillo Mantequilla](materiales/pantallas/cuchillo.md)
        - [Pantalla Malla](materiales/pantallas/malla.md)
        - [Pantalla Triple varilla](materiales/pantallas/varilla.md)
        - [Óxido de Zinc](materiales/pantallas/zinc.md)
        - [Pegamento TorSeal](materiales/pantallas/torseal.md)
        - [Motor de paso](materiales/pantallas/motor.md)
    - [Pistola de electrones](materiales/egun/main.md)
        - [Filamento](materiales/egun/filamento.md)
        - [Sonda de langmuir](materiales/egun/langmuir.md)
        - [Puerto para pruebas cualitativas](materiales/egun/puerto.md)
    - [Fuentes de poder](materiales/psu.md)
    - [Cámaras](materiales/camaras/main.md)
        - [Cámara Velociraptor HS](materiales/camaras/velociraptor.md)
        - [Cámara personal](materiales/camaras/personal.md)

- [Procedimiento](procedimiento/main.md)
    - [Preparación previa](procedimiento/preparación.md)
    - [Guía para iteración](procedimiento/iteración.md)


