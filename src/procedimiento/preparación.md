# Colocación de pantalla en puerto

## Materiales

1. Puerto de 

## Procedimiento


# Preparación de pantallas

## Materiales

1. Pincel
1. Envase de cristal, preferiblemente un breaker
1. Óxido de Zinc
1. Piseta con Acetona
1. Pantalla para mapeo (ya colocada en su puerto correspondiente)

## Procedimiento

1. Tome el envase de cristal. Límpielo y ambiente con acetona.
1. Una vez secada la acetona, vierta una cdita de óxido de zinc.
1. Vierta un mínimo de acetona en el envase. Mezcle con el pincel.
1. Repita el paso anterior hasta obtener una viscosidad similar a la de una pintura de témpera. (No muy viscoso, no muy líquido, adecuada para pegarse en superficies).
1. Sumerja el pincel en la mezcla y úntela en la pantala para mapeo. Asegúrese de cubrir toda la superficie de dicha pantalla.
1. Limpie con acetona cualquier residuo de la mezcla que haya podido haber caído sobre el puerto.
1. Asegure el puerto al chassis del stellarator


# Conexión fuentes de poder

Recuerde utilizar guantes aislantes para este procedimiento, puesto que se trabajan con altas tensiones

## Materiales

1. 2 Cables doble banana macho - crocodilo
2. 3 jumpers hembra-macho
3. 3 fuentes de poder triples (30V-3A / 30V-3A / 5V)
4. Multímetro digital















