# Advertecias previas

- Recuerde que la tensión de disparo puede estar conectada al chassis del stellarator, por lo cual debe evitar tocar el mismo durante una iteración. Apague las fuentes de alimentación si requiere tocar el chassis del reactor. 

# Guía para iteración

1. Conecte/Encienda el sistema del motor que mueve la pantalla instalada. 
1. Aseúrese de que el motor se encuentre operando con \\(T = 250ms\\), (5 pasos, 1.8°)
1. Siga los pasos de protocolo de descarga (aquí va el link al pdf) hasta el paso en que se cierra la cuchilla que alimenta al Stellarator
1. Active la cámara. Observe la luminosidad dentro del stellarator.
1. Encienda fuente(s) de poder que dan la tensión de disparo, ajuste la tensión de forma que la salida total sea de 150 V
1. Encienda la fuente que da la tensión de filamento. Ajuste la tensión 4V. Evite superar este valor, puesto que puede quemar el filamento 
1. Verifique con la cámara que el nivel de luminosidad dentro del stellarator ha aumentado. De lo contrario,detenga la iteración y siga los pasos de comprobación del filamento.
1. Ajuste el software de control para realizar una descarga de 5s en vez de 10s, de forma que el tiempo entre iteraciones se pueda reducir.
1. Realize la descarga. Comience a grabar el video cuando falten 10 segundos para el paso de corriente. 
1. Espere a que se enfríen los cables lo suficiente para poder realizar una nueva iteración
