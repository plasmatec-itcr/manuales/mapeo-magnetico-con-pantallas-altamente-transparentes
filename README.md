# Manual de Mapeo Magnético con pantallas altamente transparentes

Este repositorio contiene el código fuente del manual para el experimento de mapeo magnético. 

La documentación compilada se encuentra disponible [aquí](https://plasmatec-itcr.gitlab.io/manuales/mapeo-magnetico-con-pantallas-altamente-transparentes/)

La herramienta utilizada para crear este libro es mdBook, para maś información sobre como usarla, visitar <https://rust-lang.github.io/mdBook/>
